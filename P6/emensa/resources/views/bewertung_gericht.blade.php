@extends('emensa.emensa_layout')

@section('content')
<div style="border: thin solid black; padding: 5px 10px 5px 10px;
            width: 30%; height: 100%;">
    <h2>Bewertung für {{$gericht->name}}</h2>
    @if(isset($invalid_input) && $invalid_input == true)
        <p style="color: red">Ungültige Eingabe</p>
    @endif
    <hr>

    <form method="post" action="bewertung?gerichtid={{$gericht->id}}">
        @csrf
        @method('PUT')
        <img width="150" height="150"
             src="img/gerichte/{{$gericht->bildname ?? '00_image_missing.jpg'}}"/>
        <br>
        <input hidden value="{{$gericht->id}}" name = "gericht_id">

        <label>
            Sterne-Bewertung<br>
            <select name = 'stars'>
                <option value="0">Sehr Schlect</option>
                <option value="1">Schlect</option>
                <option value="2">Gut</option>
                <option value="3">Sehr Gut</option>
            </select>
        </label>

        <br><br>

        <label>
            Bewertung:<br>
            <textarea required name="content" style="height: 150px; width : 100%"></textarea>
        </label>

        <input type="submit">
    </form>
</div>
@endsection
