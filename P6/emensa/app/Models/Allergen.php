<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Allergen extends Model {
    use HasFactory;

    protected $primaryKey = 'code';
    protected $table = 'allergen';
    public $incrementing = false;

    protected $fillable = [
        'code',
        'name',
        'typ',
    ];

    public $timestamps = false;

    public function belongsToGericht() {
        return $this->belongsToMany(Gericht::class, 'gericht_hat_allergen', 'code', 'gericht_id');
    }
}
