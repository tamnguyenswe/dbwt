<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gericht extends Model {
    use HasFactory;

    protected $primaryKey = 'id';
    protected $table = 'gericht';

    protected $fillable = [
        'name',
        'beschreibung',
        'vegan',
        'vegetarisch',
        'preis_intern',
        'preis_extern',
        'bildname',
    ];

    public $timestamps = false;

    public function allergens() {
        return $this->belongsToMany(Allergen::class, 'gericht_hat_allergen', 'gericht_id', 'code');
    }

    public function getPreisInternAttribute($value) {
        return number_format($value, 2, ',', ' ');
    }

    public function getPreisExternAttribute($value) {
        return number_format($value, 2, ',', ' ');
    }

    public function getVeganAttribute($value) {
        return $value ? 'Ja' : 'Nein';
    }

    public function getVegetarischAttribute($value) {
        return $value ? "Ja" : "Nein";
    }
}
