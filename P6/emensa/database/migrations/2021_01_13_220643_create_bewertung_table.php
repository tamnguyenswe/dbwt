<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBewertungTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('bewertung', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('author_id')->index();
            $table->unsignedBigInteger('gericht_id')->index();
            $table->foreign('author_id')->references('user_id')->on('benutzer')
                ->cascadeOnUpdate();
            $table->foreign('gericht_id')->references('id')->on('gericht')
                ->cascadeOnUpdate();
            $table->unsignedSmallInteger('stars');
            $table->longText('content');
            $table->boolean('is_recommended')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('bewertung');
    }
}
