<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGerichtKategoriesTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('gericht_hat_kategorie', function (Blueprint $table) {
            $table->unsignedBigInteger('kategorie_id')->index();
            $table->unsignedBigInteger('gericht_id')->index();
            $table->foreign('kategorie_id')->references('id')->on('kategorie')
                ->cascadeOnDelete()->cascadeOnUpdate();
            $table->foreign('gericht_id')->references('id')->on('gericht')
                ->cascadeOnDelete()->cascadeOnUpdate();
            $table->primary(["kategorie_id", "gericht_id"]);
            $table->index(["kategorie_id", "gericht_id"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('gericht_hat_kategorie');
    }
}
