<?php

namespace Database\Seeders;

use App\Models\GerichtAllergen;
use Illuminate\Database\Seeder;

class GerichtAllergenSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $relations = [
            [
                'code' => "a1",
                'gericht_id' => 15,
            ],[
                'code' => "a2",
                'gericht_id' => 9,
            ],[
                'code' => "a2",
                'gericht_id' => 7,
            ],[
                'code' => "a3",
                'gericht_id' => 4,
            ],[
                'code' => "a3",
                'gericht_id' => 8,
            ],[
                'code' => "a3",
                'gericht_id' => 1,
            ],[
                'code' => "a4",
                'gericht_id' => 1,
            ],[
                'code' => "a4",
                'gericht_id' => 4,
            ],[
                'code' => "a4",
                'gericht_id' => 15,
            ],[
                'code' => "a5",
                'gericht_id' => 12,
            ],[
                'code' => "a6",
                'gericht_id' => 3,
            ],[
                'code' => "c",
                'gericht_id' => 7,
            ],[
                'code' => "c",
                'gericht_id' => 1,
            ],[
                'code' => "d",
                'gericht_id' => 10,
            ],[
                'code' => "d",
                'gericht_id' => 6,
            ],[
                'code' => "f",
                'gericht_id' => 10,
            ],[
                'code' => "f1",
                'gericht_id' => 3,
            ],[
                'code' => "f1",
                'gericht_id' => 1,
            ],[
                'code' => "f1",
                'gericht_id' => 4,
            ],[
                'code' => "f2",
                'gericht_id' => 12,
            ],[
                'code' => "f3",
                'gericht_id' => 15,
            ],[
                'code' => "h",
                'gericht_id' => 1,
            ],[
                'code' => "h1",
                'gericht_id' => 12,
            ],[
                'code' => "h1",
                'gericht_id' => 7,
            ],[
                'code' => "h3",
                'gericht_id' => 7,
            ],[
                'code' => "h3",
                'gericht_id' => 10,
            ],[
                'code' => "h3",
                'gericht_id' => 4,
            ],[
                'code' => "h3",
                'gericht_id' => 15,
            ],[
                'code' => "i",
                'gericht_id' => 14,
            ],[
                'code' => "i",
                'gericht_id' => 15,
            ],[
                'code' => "i",
                'gericht_id' => 3,
            ],
        ];

        foreach ($relations as $relation)
            GerichtAllergen::create($relation);
    }
}
