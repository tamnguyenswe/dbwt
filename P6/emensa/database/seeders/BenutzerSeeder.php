<?php

namespace Database\Seeders;

use App\Models\Benutzer;
use Illuminate\Database\Seeder;

class BenutzerSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Benutzer::create(
            [
                'email' => 'admin@emensa.example',
                'password' => bcrypt('admin'),
                'admin' => true,
            ]
        );
    }
}
