<?php

namespace Database\Seeders;

use App\Models\Gericht;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class GerichtSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $gerichte = [
            [
                'name' => "Bratkartoffeln mit Speck und Zwiebeln",
                'beschreibung' => "Kartoffeln mit Zwiebeln und gut Speck",
                'vegan' => true,
                'vegetarisch' => true,
                'preis_intern' => 2.3,
                'preis_extern' => 4,
                'bildname' => "01_bratkartoffel.jpg",
            ],[
                'name' => "Bratkartoffeln mit Zwiebeln",
                'beschreibung' => "Kartoffeln mit Zwiebeln und ohne Speck",
                'vegan' => true,
                'vegetarisch' => true,
                'preis_intern' => 2.3,
                'preis_extern' => 4,
                'bildname' => "03_bratkartoffel.jpg",
            ],[
                'name' => "Grilltofu",
                'beschreibung' => "Fein gewürzt und mariniert",
                'vegan' => true,
                'vegetarisch' => true,
                'preis_intern' => 2.5,
                'preis_extern' => 4.5,
                'bildname' => "03_tofu.jpg",
            ],[
                'name' => "Lasagne",
                'beschreibung' => "Klassisch mit Bolognesesoße und Creme Fraiche",
                'vegan' => false,
                'vegetarisch' => false,
                'preis_intern' => 2.5,
                'preis_extern' => 4.5,
            ],[
                'name' => "Lasagne vegetarisch",
                'beschreibung' => "Klassisch mit Sojagranulatsoße und Creme Fraiche",
                'vegan' => false,
                'vegetarisch' => true,
                'preis_intern' => 2.5,
                'preis_extern' => 4.5,
                'bildname' => "06_lasagne.jpg",
            ],[
                'name' => "Hackbraten",
                'beschreibung' => "Nicht nur für Hacker",
                'vegan' => false,
                'vegetarisch' => false,
                'preis_intern' => 2.5,
                'preis_extern' => 4,
            ],[
                'name' => "Gemüsepfanne",
                'beschreibung' => "Gesundes aus der Region, deftig angebraten",
                'vegan' => true,
                'vegetarisch' => true,
                'preis_intern' => 2.3,
                'preis_extern' => 4,
            ],[
                'name' => "Hühnersuppe",
                'beschreibung' => "Suppenhuhn trifft Petersilie",
                'vegan' => false,
                'vegetarisch' => false,
                'preis_intern' => 2,
                'preis_extern' => 3.5,
            ],[
                'name' => "Forellenfilet",
                'beschreibung' => "mit Kartoffeln und Dilldip",
                'vegan' => false,
                'vegetarisch' => false,
                'preis_intern' => 3.8,
                'preis_extern' => 5,
                'bildname' => "10_forelle.jpg",
            ],[
                'name' => "Kartoffel-Lauch-Suppe",
                'beschreibung' => "der klassische Bauchwärmer mit frischen Kräutern",
                'vegan' => false,
                'vegetarisch' => true,
                'preis_intern' => 2,
                'preis_extern' => 3,
                'bildname' => "11_soup.jpg",
            ],[
                'name' => "Kassler mit Rosmarinkartoffeln",
                'beschreibung' => "dazu Salat und Senf",
                'vegan' => false,
                'vegetarisch' => false,
                'preis_intern' => 3.8,
                'preis_extern' => 5.2,
                'bildname' => "12_kassler.jpg",
            ],[
                'name' => "Drei Reibekuchen mit Apfelmus",
                'beschreibung' => "grob geriebene Kartoffeln aus der Region",
                'vegan' => false,
                'vegetarisch' => true,
                'preis_intern' => 2.5,
                'preis_extern' => 4.5,
                'bildname' => "13_reibekuchen.jpg",
            ],[
                'name' => "Pilzpfanne",
                'beschreibung' => "die legendäre Pfanne aus Pilzen der Saison",
                'vegan' => false,
                'vegetarisch' => true,
                'preis_intern' => 3,
                'preis_extern' => 5,
            ],[
                'name' => "Pilzpfanne vegan",
                'beschreibung' => "die legendäre Pfanne aus Pilzen der Saison ohne Käse",
                'vegan' => true,
                'vegetarisch' => true,
                'preis_intern' => 3,
                'preis_extern' => 5,
                'bildname' => "15_pilze.jpg",
            ],[
                'name' => "Käsebrötchen",
                'beschreibung' => "schmeckt vor und nach dem Essen",
                'vegan' => false,
                'vegetarisch' => true,
                'preis_intern' => 1,
                'preis_extern' => 1.5,
            ],[
                'name' => "Schinkenbrötchen",
                'beschreibung' => "schmeckt auch ohne Hunger",
                'vegan' => false,
                'vegetarisch' => false,
                'preis_intern' => 1.25,
                'preis_extern' => 1.75,
                'bildname' => "17_broetchen.jpg",
            ],[
                'name' => "Tomatenbrötchen",
                'beschreibung' => "mit Schnittlauch und Zwiebeln",
                'vegan' => true,
                'vegetarisch' => true,
                'preis_intern' => 1,
                'preis_extern' => 1.5,
            ],[
                'name' => "Mousse au Chocolat",
                'beschreibung' => "sahnige schweizer Schokolade rundet jedes Essen ab",
                'vegan' => false,
                'vegetarisch' => true,
                'preis_intern' => 1.25,
                'preis_extern' => 1.75,
                'bildname' => "19_mousse.jpg",
            ],[
                'name' => "Suppenkreation á la Chef",
                'beschreibung' => "was verschafft werden muss, gut und günstig",
                'vegan' => false,
                'vegetarisch' => false,
                'preis_intern' => 0.5,
                'preis_extern' => 0.9,
                'bildname' => "20_suppe.jpg",
            ],
        ];

        foreach ($gerichte as $gericht)
            Gericht::create($gericht);
    }
}
