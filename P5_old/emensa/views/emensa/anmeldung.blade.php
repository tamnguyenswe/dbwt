@extends('emensa.emensa_layout')

@section('content')
    <div style="border: thin solid black; padding: 5px 10px 5px 10px">
        <h2>Anmeldung</h2>

        @if($failed_login)
            <p style="color: red">Email oder Passwort ist falsch</p>
        @endif
        <hr>

        <form action="/anmeldung_verfizieren" method="post">
            @csrf
            <input type="text" name="email" placeholder="Email" required>
            <br><br>
            <input type="password" name="password" placeholder="Passwort" required>
            <br><br>
            <input type="submit" value="Anmeldung">
        </form>
    </div>
@endsection