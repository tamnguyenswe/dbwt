use werbeseiteemensa;

drop table benutzer;
create table benutzer (
                        id int(8)               not null auto_increment,
                        email varchar(100)      not null unique,
                        passwort varchar(100)   not null,
                        admin boolean           ,
                        anzahlfehler int        not null,
                        anzahlanmeldungen int   not null,
                        letzteanmeldung datetime,
                        letzterfehler datetime,
                        primary key (id)
);

# insert into benutzer (email, passwort, admin, anzahlfehler, anzahlanmeldungen) value
#     ('/admin@emensa.example','admin', false, 0, 0);