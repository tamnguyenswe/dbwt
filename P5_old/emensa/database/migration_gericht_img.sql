use werbeseiteemensa;

alter table gericht
    add column bildname varchar(200) default null;

# update gericht
#     set gericht.bildname = '00_image_missing.jpg';

update gericht
    set bildname = '01_bratkartoffel.jpg'
        where id = 01;

update gericht
    set bildname = '03_bratkartoffel.jpg'
        where id = 03;

update gericht
    set bildname = '04_tofu.jpg'
        where id = 04;

update gericht
    set bildname = '06_lasagne.jpg'
        where id = 06;

update gericht
    set bildname = '10_forelle.jpg'
        where id = 10;

update gericht
    set bildname = '11_soup.jpg'
        where id = 11;

update gericht
    set bildname = '12_kassler.jpg'
        where id = 12;

update gericht
    set bildname = '13_reibekuchen.jpg'
        where id = 13;

update gericht
    set bildname = '15_pilze.jpg'
        where id = 15;

update gericht
    set bildname = '17_boretchen.jpg'
        where id = 17;

update gericht
    set bildname = '19_mousse.jpg'
        where id = 19;

update gericht
    set bildname = '20_suppe.jpg'
        where id = 20;