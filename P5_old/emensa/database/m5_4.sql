use werbeseiteemensa;

# A4.1
create view view_suppen as
    select * from gericht where name like '%suppen%';

# A4.2
create view view_anmeldungen as
    select * from benutzer order by anzahlanmeldungen;

# A4.3
create view view_kategoriegerichte_vegetarisch as
    select gericht.name as gericht_name, kat.name as kategorie_name from gericht
        left join gericht_hat_kategorie ghk on gericht.id = ghk.gericht_id
        right join kategorie kat on ghk.kategorie_id = kat.id
        where vegetarisch = true;