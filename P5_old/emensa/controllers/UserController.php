<?php
session_start();
require_once("../models/helper.php");
$_SESSION['salt'] = 'vtv3';

class UserController {
    public function verify_login(RequestData $request) {
        $connection = db_connect();

        // sanitize input fields
        $email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
        $password = filter_var($_POST['password'], FILTER_SANITIZE_EMAIL);
        $email = mysqli_real_escape_string($connection, $email);
        $password = mysqli_real_escape_string($connection, $password);
        $password = $this->salt_and_hash($password);

        mysqli_begin_transaction($connection);

        // prepare statements
        $statement_user_check = mysqli_stmt_init($connection);
        mysqli_stmt_prepare($statement_user_check, "SELECT * FROM benutzer WHERE email = (?) AND passwort = (?)");
        mysqli_stmt_bind_param($statement_user_check, 'ss', $email, $password);

        mysqli_stmt_execute($statement_user_check);
        $result = mysqli_stmt_fetch($statement_user_check);

        if ($result === null) {
            // if no result found, which means something went wrong, redirect back to login page
            $this->login_fail($email);
        } else if ($result == true) {
            // logged in successfully
            $this->login_success($email);
        } else {
            // if false, which means something went wrong, redirect back to login page
            $this->login_fail($email);
        }

        mysqli_commit($connection);
    }


    /**
     * Show the login page
     * @param RequestData $request
     * @return string
     */
    public function login(RequestData $request) {
        return view('emensa.anmeldung', ['failed_login' => $_SESSION['failed_login']]);
    }


    /**
     * Logout if logged in, destroy current session
     * @param RequestData $request
     */
    public function logout(RequestData $request) {
        if ($_SESSION['user_email'] !== null) {
            $user_email = $_SESSION['user_email'];
            get_logger()->info("$user_email logged out");
        }

        // destroy this session
        session_destroy();
        session_regenerate_id();
        header("Location: /"); // redirect back to index page

    }


    /**
     * After a successful login, redirect to index page, increase login counter by 1, write log for login time
     * @param string $email The email that successfully logged in
     */
    private function login_success(string $email) {
        $connection = db_connect();

        header("Location: /");
        $_SESSION['failed_login'] = false;
        $_SESSION['user_email'] = $email;

        // prepare statements for increasing login counter
        $statement_login_counter = mysqli_stmt_init($connection);
        mysqli_stmt_prepare($statement_login_counter,
            "SELECT id FROM benutzer WHERE email = (?)");
        mysqli_stmt_bind_param($statement_login_counter, 's', $email);
        mysqli_stmt_execute($statement_login_counter);

        // get benutzer id from email
        $benutzer_id = mysqli_stmt_get_result($statement_login_counter)->fetch_array()[0];
        $query = "CALL inc_login_counter($benutzer_id)";
        mysqli_query($connection, $query);

        // prepare statements for write login time
        $datetime = get_time();
        $statement_login_time = mysqli_stmt_init($connection);
        mysqli_stmt_prepare($statement_login_time,
            "UPDATE benutzer SET letzteanmeldung = (?) WHERE email = (?)");
        mysqli_stmt_bind_param($statement_login_time, 'ss', $datetime, $email);
        mysqli_stmt_execute($statement_login_time);

        // write log
        get_logger()->info("$email logged in");
    }


    /**
     * After a failed login, redirect back to login page, write log for the failed attempt
     * @param string $email
     */
    private function login_fail(string $email) {
        $connection = db_connect();

        header("Location: /anmeldung");
        $_SESSION['failed_login'] = true;

        // prepare statements for write login time
        $datetime = get_time();
        $statement_login_time_failed = mysqli_stmt_init($connection);
        mysqli_stmt_prepare($statement_login_time_failed,
            "UPDATE benutzer SET letzterfehler = (?) WHERE email = (?)");
        mysqli_stmt_bind_param($statement_login_time_failed, 'ss', $datetime, $email);
        mysqli_stmt_execute($statement_login_time_failed);

        // write log
        get_logger()->warning("$email tried to log in but failed");
    }


    /**
     * Add salt and hash a string with sha256 algorithm
     *
     * @param string $str The string to hash with
     * @return string The salted and hashed string
     */
    private function salt_and_hash(string $str) : string {
        $salted = $str.$_SESSION['salt'];
        return hash('sha256', $salted);
    }


    /**
     * Register an admin account in the benutzer table.
     */
    public function admin_register() {
        $password = $this->salt_and_hash('admin');

        $query = "INSERT INTO benutzer (email, passwort, admin, anzahlfehler, anzahlanmeldungen) 
                    VALUE ('admin@emensa.example','{$password}', true, 0, 0)";
        $connection = db_connect();

        $result = mysqli_query($connection, $query);

        if ($result === null) {
            echo "something went wrong";
            write_error_log(mysqli_error($connection));
        } else
            echo "done";

    }

}