<?php
/**
 * Write an error message with timestamp into a log file
 * @param string $error_msg The error message to write down
 */
function write_error_log(string $error_msg) {
    $time = date('d.m.Y H:i:s ');
    $timezone = date_default_timezone_get() . " | ";
    file_put_contents("./error_logs.txt", $time . $timezone . $error_msg . "\n", FILE_APPEND);
}


/**
 * Check if a newsletter registration is valid.
 * @param array $post_val $_POST array to get the POST values from.
 * @param mysqli $connection The connection being used to connect to db
 * @return bool True if successfully record a new newsletter registration, otherwise false
 */
function register_newsletter(array $post_val, mysqli $connection) : bool {
    $email = trim($post_val['email']);
    $vorname = trim($post_val['vorname']);
    $language = $post_val['language'];

    $email = filter_var($email, FILTER_SANITIZE_EMAIL);
    $vorname = filter_var($vorname, FILTER_SANITIZE_STRING);

    // break if invalid entry
    $is_valid =  !(($vorname == "") ||
        (substr_count($email, '@') != 1) ||
        (substr_count($email, '.') < 1));

    if (!$is_valid)
        return false;

    $invalid_emails = ["@trashmail", "@rcpt.at", "@damnthespam.at", "@wegwerfmail.at"];

    foreach ($invalid_emails as $invalid_word) {
        if (strpos($email, $invalid_word)) {
            $is_valid = false;
            break;
        }
    }

    if (!$is_valid)
        return false;

    $query = "SELECT email FROM newsletter";
    $result = mysqli_query($connection, $query);

    if (!$result) {
        write_error_log(mysqli_error($connection));
        return false;
    }
    // else the entry should be a valid one.
    $email_list = array_values(mysqli_fetch_assoc($result));

    // omit if this email address already exists in db
    if (in_array($email, $email_list))
        return false;

    $query = "INSERT INTO newsletter VALUE ('${email}', '${vorname}', '${language}')";
    if (mysqli_query($connection, $query)) {
        return true;
    } else {
        write_error_log(mysqli_error($connection));
        return false;
    }
}

/**
 * Connect to the db
 * @return mysqli The connection to the db
 */
function db_connect() : mysqli {
    $servername = "127.0.0.1";
    $username = "user";
    $password = "";
    $database = "werbeseiteemensa";

    return mysqli_connect($servername, $username, $password, $database,3306);
}


/**
 * @return false|string Current time formatted to be compatible with sql
 */
function get_time() {
    date_default_timezone_set('Europe/Berlin');
    return date('Y-m-d H:i:s', time());
}
