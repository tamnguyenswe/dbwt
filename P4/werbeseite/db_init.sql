-- SET NAMES utf8mb4_unicode_ci

create schema werbeseiteemensa;

use werbeseiteemensa;

create table allergen (
    code    char(4)         not null primary key ,
    name    varchar(300)    null ,
    typ     varchar(20)     null
);

create table gericht (
    id              int(8)          not null primary key ,
    name            varchar(80)     not null unique ,
    beschreibung    varchar(800)    not null ,
    erfasst_am      date            not null ,
    vegan           bool            not null ,
    vegetarisch     bool            not null ,
    preis_intern    double          not null ,
    preis_extern    double          not null ,
    check ( preis_extern > preis_intern )
);
# (`id`, `name`, `beschreibung`, `erfasst_am`, `vegan`, `vegetarisch`, `preis_intern`, `preis_extern`)
create table gericht_hat_allergen (
    code        char(4) ,
    gericht_id  int(8) not null references gericht(id)
);

create table kategorie (
    id          int(8)          not null primary key ,
    eltern_id   int(8) ,
    name        varchar(80) ,
    bildname    varchar(200)
);

create table gericht_hat_kategorie (
    kategorie_id    int(8) not null references kategorie(id),
    gericht_id      int(8) not null references gericht(id)
);

create table counter (
    counter_name    varchar(255)    not null primary key ,
    value           int(8)          not null
);

insert into counter value ('visits', 0);
insert into counter value ('gericht', 0);
insert into counter value ('newsletter', 0);

create table newsletter (
    email       varchar(255)    not null primary key ,
    name        varchar(255)    not null ,
    language    varchar(2)      not null
)