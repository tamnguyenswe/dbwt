<!--Praktikum DBWT. Autoren:-->
<!--    Nguyen, Duc Tam, 3233521-->
<!--    Tran, Anh Minh, 3246773-->


<?php

/**
 * Connect to the db
 * @return mysqli The connection to the db
 */
function db_connect() : mysqli {
    $servername = "127.0.0.1";
    $username = "user";
    $password = "";
    $database = "werbeseiteemensa";

    return mysqli_connect($servername, $username, $password, $database,3306);
}


/**
 * Check if a newsletter registration is valid.
 * @param array $post_val $_POST array to get the POST values from.
 * @param mysqli $connection The connection being used to connect to db
 * @return bool True if successfully record a new newsletter registration, otherwise false
 */
function register_newsletter(array $post_val, mysqli $connection) : bool {
    $email = trim($post_val['email']);
    $vorname = trim($post_val['vorname']);
    $language = $post_val['language'];

    $email = filter_var($email, FILTER_SANITIZE_EMAIL);
    $vorname = filter_var($vorname, FILTER_SANITIZE_STRING);

    // break if invalid entry
    $is_valid =  !(($vorname == "") ||
        (substr_count($email, '@') != 1) ||
        (substr_count($email, '.') < 1));

    if (!$is_valid)
        return false;

    $invalid_emails = ["@trashmail", "@rcpt.at", "@damnthespam.at", "@wegwerfmail.at"];

    foreach ($invalid_emails as $invalid_word) {
        if (strpos($email, $invalid_word)) {
            $is_valid = false;
            break;
        }
    }

    if (!$is_valid)
        return false;

    $query = "SELECT email FROM newsletter";
    $result = mysqli_query($connection, $query);

    if (!$result) {
        write_error_log(mysqli_error($connection));
        return false;
    }
    // else the entry should be a valid one.
    $email_list = array_values(mysqli_fetch_assoc($result));

    // omit if this email address already exists in db
    if (in_array($email, $email_list))
        return false;
    /**
     * Security: Eingabemaskierung für email und vorname vor INSERT hinfügen
     */
    $email = mysqli_real_escape_string($connection,$email);
    $vorname = mysqli_real_escape_string($connection,$vorname);

    /* -- old code
    Daten in Datenbank hinfügen

    $query = "INSERT INTO newsletter VALUE ('${email}', '${vorname}', '${language}')";
    if (mysqli_query($connection, $query)) {
        return true;
    } else {
        write_error_log(mysqli_error($connection));
        return false;
    }
    */

    /**
     * Security: Statement vorbereiten, Data in Datenbank hinfügen
     */
    $statement = mysqli_stmt_init($connection);
    mysqli_stmt_prepare($statement, "INSERT INTO newsletter VALUES(?,?,?)");
    mysqli_stmt_bind_param($statement,'sss',$email,$vorname,$language);
    if(mysqli_stmt_execute($statement)){
        return true;
    }else{
        write_error_log(mysqli_error($connection));
        return false;
    }
    mysqli_stmt_close($statement);
}


/**
 * Increase a counter in db by one
 * @param mysqli $connection The connection being used to connect to db
 */
function increase_visit_counter(mysqli $connection) {
    $query = "SELECT value FROM counter WHERE counter_name = 'visits'";

    $result = mysqli_query($connection, $query);

    if ($result) {
        $counter_value = (int) mysqli_fetch_assoc($result)['value'];
        $counter_value++;

        $query = "UPDATE counter SET value = ${counter_value} WHERE counter_name = 'visits'";
        if (mysqli_query($connection, $query)) {
            return;
        } else {
            write_error_log(mysqli_error($connection));
        }
    } else {
        write_error_log(mysqli_error($connection));
    }
}


/**
 * Get a counter value from db
 * @param mysqli $connection The connection being used to connect to db
 * @param string $counter_name The counter to be increased
 * @return int The counter's value
 */
function get_counter_value(mysqli $connection, string $counter_name) : int {
    switch ($counter_name) {
        case "visits":
            $query = "SELECT value FROM counter WHERE counter_name = '${counter_name}'";
            $result = mysqli_query($connection, $query);

            if ($result)
                return mysqli_fetch_assoc($result)['value'];
            else
                write_error_log(mysqli_error($connection));
            break;

        case "newsletter":
            $query = "SELECT COUNT(email) FROM newsletter";
            $result = mysqli_query($connection, $query);

            if ($result)
                return array_values(mysqli_fetch_assoc($result))[0];
            else
                write_error_log(mysqli_error($connection));
            break;

        case "meals":
            $query = "SELECT COUNT(id) FROM gericht";
            $result = mysqli_query($connection, $query);

            if ($result)
                return array_values(mysqli_fetch_assoc($result))[0];
            else
                write_error_log(mysqli_error($connection));
            break;
        default:
            return 0;
    }
}


/**
 * Write an error message with timestamp into a log file
 * @param string $error_msg The error message to write down
 */
function write_error_log(string $error_msg) {
    $time = date('d.m.Y H:i:s ');
    $timezone = date_default_timezone_get() . " | ";
    file_put_contents("./error_logs.txt", $time . $timezone . $error_msg . "\n", FILE_APPEND);
}