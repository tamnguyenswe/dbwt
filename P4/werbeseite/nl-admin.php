<!--Praktikum DBWT. Autoren:-->
<!--    Nguyen, Duc Tam, 3233521-->
<!--    Tran, Anh Minh, 3246773-->

<?php

require ("model.php");
$connection = db_connect();

//sort by name as default
$sort = $_GET['sort'] ?? "name";
$filter_value = $_GET['filter'] ?? "";

$query = "SELECT * FROM newsletter ORDER BY ${sort}";

?>

<!DOCTYPE html>
<html lang="de">
    <head>
        <title>Newsletter Administration</title>
        <meta charset="UTF-8">
        <style type="text/css">
            * {
                font-family: Arial, serif;
            }

            table {
                border: solid thin black;
                border-collapse: collapse;
            }

            td, th {
                border: solid thin black;
                padding: 2px 10px 2px 10px;
            }
            /*}*/
        </style>
    </head>

    <body>
        <form action="nl-admin.php" method="get">
            <input type="text" name="filter" id="filter" placeholder="Filter">
            <input type="hidden" name="sort" value="<?php echo $sort?>">
            <input type="submit" value="Search">
        </form>

        <table>
            <thead>
                <tr>
                    <th>
                        <a
                        <?php echo 'href="./nl-admin.php?filter='.htmlspecialchars($filter_value).'&sort=email"'?>
                        >Email</a>
                    </th>

                    <th>
                        <a
                        <?php echo '<a href="./nl-admin.php?filter='.htmlspecialchars($filter_value).'&sort=name"'?>
                        >Name</a>
                    </th>

                    <th>
                        <a
                        <?php echo '<a href="./nl-admin.php?filter='.htmlspecialchars($filter_value).'&sort=language"'?>
                        >Sprache</a>
                    </th>
                </tr>
            </thead>

            <tbody>
                <?php
                    $result = mysqli_query($connection, $query);
                    if ($result) {
                        $arr = mysqli_fetch_all($result);
                        foreach ($arr as $elem) {

                            $email = $elem[0];
                            $name = $elem[1];
                            $language = $elem[2];

                            if ($filter_value != "")
                                if (strpos($name, $filter_value) === false)
                                    continue;

                            echo "<tr>";
                            echo "<td>${email}</td>";
                            echo "<td>${name}</td>";
                            echo "<td>${language}</td>";
                            echo "</tr>";
                        }
                    } else
                        write_error_log(mysqli_error($connection));
                ?>

            </tbody>
        </table>
    </body>
</html>
