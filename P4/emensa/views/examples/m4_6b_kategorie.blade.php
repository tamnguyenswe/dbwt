@extends('m4_a6_layout')
@section('title', 'Kategorie')

@section('style')
    body {
        background-color: {{$rd->query['bgcolor'] ?? 'ffffff'}};
        font-family: "Arial", serif;
    }

    table {
        border-collapse: collapse;
    }
    th, td {
        border: black thin solid;
        padding: 0 5px 0 5px;
        text-align: center;
    }

    tr:nth-child(odd) {
        font-weight: bold;
    }
@endsection

@section('content')
    <table>
        <thead>
        <tr>
            <th>ID</th>
            <th>Kategoriename</th>
        </tr>
        </thead>

        <tbody>
        @foreach($categories as $category)
            <tr>
                <td>{{$category['id']}}</td>
                <td>{{$category['name']}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection
