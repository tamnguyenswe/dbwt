<?php
$famousMeals = [
    1 => ['name'    => 'Currywurst mit Pommes',
          'winner'  => [2001, 2003, 2007, 2010, 2020]],
    2 => ['name'    => 'Hähnchencrossiesmit Paprikareis',
          'winner'  => [2002, 2004, 2008]],
    3 => ['name'    => 'Spaghetti Bolognese',
          'winner'  => [2011, 2012, 2017]],
    4 => ['name'    => 'Jägerschnitzel mit Pommes',
          'winner'  => 2019]];

function noWinner() {
    $years = [];
    global $famousMeals;

//    Mark all years as true
    for ($i = 0; $i < 21; $i++) {
        array_push($years, true);
    }

//    Mark all years with a winner as false
    foreach ($famousMeals as $entry) {
        if (gettype($entry['winner']) == "array") {
            foreach ($entry['winner'] as $year) {
                $years[$year - 2000] = false;
            }
        } else {
            $year = $entry['winner'];
            $years[$year - 2000] = false;
        }
    }

    $result = [];
    for ($i = 0; $i < 21; $i++) {
        if ($years[$i])
            array_push($result, $i + 2000);
    }
//    echo print_r($result);
    return $result;
}

//noWinner();
?>
<!DOCTYPE html>
<html lang="de">
    <head>
        <meta charset="UTF-8">
        <title>P2A5d</title>
        <style>
            * {
                font-family: Arial;
            }
            li {
                padding-bottom: 5px;
            }

            span {
                position: relative;
                left: 10px;
            }
        </style>
    </head>

    <body>
        <div>
            <ol>
                <?php
                    foreach ($famousMeals as $entry) {
                        echo "<li><span>".$entry['name'].'<br>';

                        if (gettype($entry['winner']) == "array") {
                            for ($i = count($entry['winner']) - 1; $i > 0; $i--) {
                                echo $entry['winner'][$i].", ";
                            }
                            echo $entry['winner'][0];
                        } else {
                            echo $entry['winner'];
                        }

                        echo "</span></li>";
                    }
                ?>
            </ol>
        </div>
    </body>
</html>
