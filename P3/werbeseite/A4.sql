# Praktikum DBWT. Autoren:
#     Nguyen, Duc Tam, 3233521
#     Tran, Anh Minh, 3246773
#


use werbeseiteemensa;

select * from allergen;

select * from gericht_hat_allergen;

select * from gericht;

select * from gericht_hat_kategorie;

select * from kategorie;

show columns from gericht;

select count(*) from allergen;
select count(*) from gericht_hat_kategorie;
select count(*) from gericht_hat_allergen;
select count(*) from gericht;
select count(*) from kategorie;