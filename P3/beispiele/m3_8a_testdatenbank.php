<?php
$servername = "localhost";
$username = "user";
$password = "";
$database = "werbeseiteemensa";

$connection = mysqli_connect($servername, $username, $password, $database);

$query = "SELECT column_name FROM information_schema.COLUMNS WHERE table_name = 'gericht'";
$result = mysqli_query($connection, $query);

//var_dump($result);

$column_names = [];

// get column names
while ($column = mysqli_fetch_assoc($result)) {
    array_push($column_names, $column['column_name']);
}

$query = "SELECT * FROM gericht";
$result = mysqli_query($connection, $query);

?>

<!DOCTYPE html>
<html lang="de">
    <head>
        <title>Aufgabe 6</title>
    </head>

    <style>
        * {
            font-family: Arial, serif;
        }

        table {
            border-collapse: collapse;
        }
        th, td {
            border: thin solid black;
            padding: 2px 10px 2px 10px;
        }
    </style>

    <body>
        <table>
            <thead>
                <tr>
                    <?php
                    foreach ($column_names as $column) {
                        echo "<th>${column}</th>";
                    }
                    ?>
                </tr>
            </thead>

            <tbody>
                <?php
                    while ($val = mysqli_fetch_assoc($result)) {
                        echo "<tr>";
                        foreach ($val as $foo) {
                            echo "<td>${foo}</td>";
                        }
                        echo "</tr>";
                    }
                ?>
            </tbody>
        </table>
    </body>

</html>