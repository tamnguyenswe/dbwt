@extends('m4_a6_layout')

@section('title', 'Gerichte')
@section('style')
    table {
        border-collapse: collapse;
    }
    th, td {
        border: thin black solid;
        padding: 2px 5px 2px 5px;
    }
@endsection

@section('content')
    @if(count($dishes) > 0)
        <table>
            <thead>
                <tr>
                    <th>Namen</th>
                    <th>Interne Preise</th>
                </tr>
            </thead>
            <tbody>
                @foreach($dishes as $dish)
                    <tr>
                        <td>{{$dish['name']}}</td>
                        <td>{{$dish['preis_intern']}}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @else
        <p>Es sind keine Gerichte vorhanden</p>
    @endif

@endsection