<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GerichtAllergen extends Model {
    protected $table = 'gericht_hat_allergen';
    protected $primaryKey = ['code', 'gericht_id'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code',
        'gericht_id'
    ];
    use HasFactory;
}
