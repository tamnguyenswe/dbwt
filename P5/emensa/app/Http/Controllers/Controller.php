<?php

namespace App\Http\Controllers;

use App\Models\Counter;
use App\Models\Gericht;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Request;

class Controller extends BaseController {
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index(Request $request) {
        $this->increaseCounter('visits');
        $gerichte = Gericht::all()->take(5);
        dd($gerichte[0]->getAllergen);
    }

    /**
     * Increase the counter value of a given counter by 1
     *
     * @param string $name Name of the counter
     */
    private function increaseCounter(string $name) {
        $counter = Counter::firstWhere('counter_name', $name);
        if ($counter) {
            $counter->update(['value' => $counter['value'] + 1]);
        }
    }

}
